import { componentNode } from '../components/esri-map/esri-map.component'
import { Components } from './component'

export class DoubleTree {
    public components:Components[]
    public nodes:componentNode[]
}
