import { Roles } from './roles'
import { Component } from '@angular/core'

export class Responsible{
   id:number
   role:number

   name:string
   responsiblePassword:string
}