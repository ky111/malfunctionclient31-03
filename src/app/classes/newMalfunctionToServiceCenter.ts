export class NewMalfunctionsToServiceCenter {
    malfunctionDTO_id: number
    malfunctionStatusDTO_name: string
    componentDTO_name: string
    malfunctionDTO_description: string
    urgencyDTO_name: string
}