export class Urgency {
    public id:number
    public urgencyName:string
    constructor(id:number,urgencyName:string){
        this.id=id
        this.urgencyName=urgencyName
    }
}
